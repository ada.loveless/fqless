use anyhow::Result;
use std::path::Path;
use std::{
    fs::File,
    io::Read,
};
use flate2::read::GzDecoder;

use super::fastq::{
    Record,
    Parser
};

pub struct Reader {
    reader: Box<dyn Read>, 
    parser: Parser,
}

impl Reader {
    pub fn new(reader: impl Read + 'static) -> Self {
        let parser = Parser::new();
        let reader = Reader {
            reader: Box::new(reader), 
            parser,
        };
        reader
    }
}

impl Reader {
    pub fn read(&mut self) -> Result<Vec<Record>> {
        let mut buffer = String::new();
        self.reader.read_to_string(&mut buffer)?;
        let records = self.parser.parse(&buffer)?;
        Ok(records)
    }
}

pub fn reader(handle: &str) -> Result<Reader> {
    let file = File::open(handle)?;
    let reader = match Path::new(&handle).extension() {
        Some(end) => {
            match end.to_str() {
                Some("gz") => Reader::new(GzDecoder::new(file)),
                Some("gzip") => Reader::new(GzDecoder::new(file)),
                Some(_) => Reader::new(file),
                None => Reader::new(file),
            }
        },
        None => Reader::new(file),
    };
    Ok(reader)
}

fn read_from_plain(handle: &str) -> Result<Vec<Record>> {
    let mut file = File::open(handle)?;
    let mut reader = Reader::new(file);
    reader.read()
}

fn read_from_gzip(handle: &str) -> Result<Vec<Record>> {
    let mut file = File::open(handle)?;
    let mut decoder = GzDecoder::new(file);
    let mut reader = Reader::new(decoder);
    reader.read()
}

pub fn read_records(handle: &str) -> Result<Vec<Record>> {
    match Path::new(&handle).extension() {
        Some(end) => {
            match end.to_str() {
                Some("gz") => read_from_gzip(handle),
                Some("gzip") => read_from_gzip(handle),
                Some(_) => read_from_plain(handle),
                None => read_from_plain(handle),
            }
        },
        None => read_from_plain(handle),
    }
}


#[test]
fn test_read_records() {
    let mut file = File::open("sample.fastq").unwrap();
    let mut reader = Reader::new(file);
    let records = reader.read().unwrap();
    assert!(records.len() == 3);
}
