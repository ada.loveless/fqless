use ratatui::{
    prelude::*,
    widgets::*,
};
use super::fastq::Record;

pub trait Next {
    fn next(self) -> Self;
}

#[derive(Debug, Clone, Copy)]
pub enum SeqColour {
    Plain,
    GATC,
    Quality,
}

impl Next for SeqColour {
    fn next(self) -> Self {
        match self {
            SeqColour::Plain => SeqColour::GATC,
            SeqColour::GATC => SeqColour::Quality,
            SeqColour::Quality => SeqColour::Plain,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum ViewMode {
    Plain,
    SeqOnly,
    SeqName,
    Concise,
}

impl Next for ViewMode {
    fn next(self) -> Self{
        match self {
            ViewMode::Plain => ViewMode::SeqOnly,
            ViewMode::SeqOnly => ViewMode::SeqName,
            ViewMode::SeqName => ViewMode::Concise,
            ViewMode::Concise => ViewMode::Plain,
        }
    }
}

pub struct Footline {
    mode: ViewMode,
    pos: u16,
    max: u16,
}

impl Footline {
    pub fn new(mode: ViewMode, pos: u16, max: u16) -> Self {
        Self{mode, pos, max}
    }
}

impl Widget for Footline {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let style = Style::default()
            .add_modifier(Modifier::REVERSED);
        let content = format!("{:?} [{}/{}]", 
                                self.mode, 
                                self.pos+1, 
                                self.max+1);
        let text = Text::from(content);
        let foot = Paragraph::new(text)
            .alignment(Alignment::Right)
            .style(style);
        foot.render(area, buf)
    }
}

pub struct Headline {
    title: String
}

impl Headline {
    pub fn new(title: String) -> Self {
        Self{title}
    }
}

impl Widget for Headline {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let style = Style::default()
            .add_modifier(Modifier::REVERSED);
        let text = Text::from(format!("{}:", self.title));
        let title = Paragraph::new(text)
            .style(style);
        title.render(area, buf)
    }
}

pub struct RecordsWidget<'a> {
    records: &'a Vec<Record>,
    seq_colour: SeqColour,
    qual_colour: SeqColour,
    view: ViewMode,
    scroll: u16,
}

impl<'a> RecordsWidget<'a> {
    pub fn new(records: &'a Vec<Record>) -> Self {
        RecordsWidget {
            records,
            seq_colour: SeqColour::Plain,
            qual_colour: SeqColour::Plain,
            view: ViewMode::Plain,
            scroll: 0,
        }
    }
    pub fn scroll(mut self, x: u16) -> Self {
        self.scroll = x;
        self
    }
    pub fn style(mut self, seq_colour: SeqColour, qual_colour: SeqColour, view: ViewMode) -> Self {
        self.seq_colour = seq_colour;
        self.qual_colour = qual_colour;
        self.view = view;
        self
    }
    fn format_record<'b>(&self, record: &Record) -> Vec<Line<'b>> {
        match self.view {
            ViewMode::SeqOnly => {
                vec!(self.format_seq(record))
            },
            ViewMode::Concise => {
                vec!(self.format_seq_name(record))
            },
            ViewMode::SeqName => {
                vec![
                    self.format_name(record),
                    self.format_seq(record),
                ]
            }
            _ => {
                vec![
                    self.format_name(record),
                    self.format_seq(record),
                    Line::from("+"),
                    self.format_qual(record),
                ]
            }
        }
    }
    fn format_seq<'b>(&self, record: &Record) -> Line<'b> {
        let string = &record.seq;
        let spans = match self.seq_colour {
            SeqColour::GATC => format_str_gatc(string, string),
            SeqColour::Quality => format_str_cont(string, &record.qual),
            _ => format_str_plain(&string),
        };
        Line::from(spans)
    }
    fn format_seq_name<'b>(&self, record: &Record) -> Line<'b> {
        let mut spans = format_str_plain(&format!("@{} ", record.seqid));
        let string = &record.seq;
        let mut span_name = match self.seq_colour {
            SeqColour::GATC => format_str_gatc(string, string),
            SeqColour::Quality => format_str_cont(string, &record.qual),
            _ => format_str_plain(&string),
        };
        spans.append(&mut span_name);
        Line::from(spans)
    }
    fn format_name<'b>(&self, record: &Record) -> Line<'b> {
        Line::from(format_str_plain(&format!("@{}", record.seqid)))
    }
    fn format_qual<'b>(&self, record: &Record) -> Line<'b> {
        let string = String::from_utf8(record.qual.clone()).unwrap();
        let spans = match self.qual_colour {
            SeqColour::GATC => format_str_gatc(&string, &record.seq),
            SeqColour::Quality => format_str_cont(&string, &record.qual),
            _ => format_str_plain(&string),
        };
        Line::from(spans)
    }
}

fn format_str_plain<'a>(string: &String) -> Vec<Span<'a>> {
    let style = Style::default()
       .bg(Color::Reset)
       .fg(Color::Reset);
    vec!(Span::styled(string.clone(), style))
}

fn format_str_gatc<'a>(string: &String, seq: &String) -> Vec<Span<'a>> {
    let chars = string.chars();
    let levels = seq.chars();
    let mut content = Vec::new();
    for (level, char) in levels.zip(chars) {
        let color = match level {
            'G' => Color::Yellow,
            'A' => Color::LightBlue,
            'C' => Color::Red,
            'T' => Color::Green,
            'N' => Color::Gray,
            _ => Color::Reset,
        };
        let span = Span::styled(char.to_string(), Style::default().fg(color));
        content.push(span);
    }
    content
}

fn format_str_cont<'a>(string: &String, value: &Vec<u8>) -> Vec<Span<'a>> {
    let mut content = Vec::new();
    for (char, qual) in string.chars().zip(value.iter()) {
        let v = qual.clone();
        let color = Color::Rgb(v, v, v);
        let span = Span::styled(char.to_string(), Style::default().fg(color));
        content.push(span);
    }
    content
}


impl<'a> Widget for RecordsWidget<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let mut entries = Vec::new();
        for record in self.records {
            let lines = self.format_record(&record);
            for line in lines {
                entries.push(line);
            }
        }
        let paragraph = Paragraph::new(entries)
            .scroll((self.scroll, 0));
        paragraph.render(area, buf)
    }
}
