use std::{
    io::{self, Read},
    time::{Duration, Instant}, fs::File,
    //fs::File, 
    //io::{Read, Stdout}, thread, 
};
use anyhow::Result;
use ratatui::{
    Terminal, 
    backend::CrosstermBackend, 
    prelude::*,
};
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};

use super::reader::{read_records, Reader};
use super::fastq::Record;
use super::traits::Random;
use super::widgets::{RecordsWidget, Headline, Footline, Next, SeqColour, ViewMode};

pub struct Viewer {
    reader: Reader,
    records: Vec<Record>,
    title: String,
    seq_colour: SeqColour,
    qual_colour: SeqColour,
    view_mode: ViewMode,
    index: u16,
}

impl Viewer {
    pub fn new(title: &str, reader: Reader) -> Result<Self> 
    {
        let viewer = Viewer {
            reader, 
            title: String::from(title),
            seq_colour: SeqColour::Plain,
            qual_colour: SeqColour::Plain,
            view_mode: ViewMode::SeqOnly,
            records: Vec::new(), 
            index: 0
        };
        Ok(viewer)
    }
    fn up(&mut self) {
        if self.index < self.records.len() as u16-1 {
            self.index += 1;
        }
    }
    fn top(&mut self) {
        self.index = 0;
    }
    fn bottom(&mut self) {
        self.index = self.records.len() as u16-1;
    }
    fn down(&mut self) {
        if self.index > 0 {
            self.index -= 1;
        }
    }
}

fn run_viewer<B: Backend>(terminal: &mut Terminal<B>, viewer: &mut Viewer, tick_rate: Duration) -> Result<()> {
    let mut last_tick = Instant::now();
    viewer.reader.read().into_iter().for_each(|mut record| {
        viewer.records.append(&mut record);
    });
    loop {
        terminal.draw(|f| ui(f, &viewer))?;
        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                match key.code {
                    KeyCode::Char('q') => return Ok(()),
                    KeyCode::Char('k') => viewer.down(),
                    KeyCode::Char('j') => viewer.up(),
                    KeyCode::Char('G') => viewer.bottom(),
                    KeyCode::Char('g') => viewer.top(),
                    KeyCode::Char('1') => viewer.view_mode = viewer.view_mode.next(),
                    KeyCode::Char('2') => viewer.seq_colour = viewer.seq_colour.next(),
                    KeyCode::Char('3') => viewer.qual_colour = viewer.qual_colour.next(),
                    _ => {},
                }
            }
        }
        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now();
        }
    }
}

fn ui<B>(f: &mut Frame<B>, 
            viewer: &Viewer) 
where B: Backend,
{
    let size = f.size();
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
             Constraint::Length(1),
             Constraint::Min(1),
             Constraint::Length(1),
        ])
        .split(size);
    let title = Headline::new(viewer.title.clone());
    let foot = Footline::new(viewer.view_mode, viewer.index, viewer.records.len() as u16);
    let records = RecordsWidget::new(&viewer.records)
        .style(viewer.seq_colour, viewer.qual_colour, viewer.view_mode)
        .scroll(viewer.index);
    f.render_widget(title, layout[0]);
    f.render_widget(records, layout[1]); 
    f.render_widget(foot, layout[2]);
}

pub fn main(handle: &str) -> Result<String> {
    // Setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // Setup viewer
    let tick_rate = Duration::from_millis(250);
    let file = File::open(&handle)?;
    let reader = Reader::new(file);
    let mut viewer = Viewer::new(&handle, reader)?;
    let res = run_viewer(&mut terminal, &mut viewer, tick_rate);
    //Viewer::from_records(records);
    
    // Restore terminal
    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen, DisableMouseCapture)?;
    terminal.show_cursor()?;

    res?;
    
    Ok(String::from("EXIT"))
}
