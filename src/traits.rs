pub trait Random {
    fn rand(len: usize) -> Self;
}
