use regex::{Regex, Captures};
use anyhow::Result;
use rand::prelude::*;
use rand::distributions::Alphanumeric;
use super::traits::Random;

type Quality = Vec<u8>;

const RE_ENTRY: &str = r"(?x)
                       @(?<seqid>[A-Za-z0-9_.:-]+)\n
                       (?<seq>[A-Za-z\n\.~]+)\n
                       \+(?:[A-Za-z0-9_.:-])*\n
                       (?<qual>[!-~]+)";
const RE_SEQ_NEG: &str = r"[^A-Za-z]+";
const RE_QUAL_NEG: &str = r"[^!-~]+";

pub struct Parser {
    re_entry: Regex,
    re_qual_neg: Regex,
    re_seq_neg: Regex,
}

impl Parser {
    pub fn new() -> Self {
        Parser {
            re_entry: Regex::new(RE_ENTRY).unwrap(),
            re_seq_neg: Regex::new(RE_SEQ_NEG).unwrap(),
            re_qual_neg: Regex::new(RE_QUAL_NEG).unwrap(),
        }
    }
    fn read_sequence_to_string(&self, raw: &str) -> String {
        String::from(self.re_seq_neg.replace_all(raw, ""))
    }
    fn read_quality_to_bytes(&self, raw: &str) -> Quality {
        self.re_qual_neg.replace_all(raw, "").as_bytes().to_vec()
    }
    fn read_capture(&self, capture: Captures) -> Record {
         Record {
            seqid: String::from(&capture["seqid"]),
            seq: self.read_sequence_to_string(&capture["seq"]),
            qual: self.read_quality_to_bytes(&capture["qual"]),
        }
    }
    pub fn parse_record(&self, buffer: &str) -> Result<Record> {
        match self.re_entry.captures(buffer) {
            Some(entry) => Ok(self.read_capture(entry)),
            None => anyhow::bail!("Tried to parse invalid record:\n{}", buffer)
        }
    }
    pub fn parse(&self, buffer: &str) -> Result<Vec<Record>> {
        let mut records = Vec::new();
        for capture in self.re_entry.captures_iter(&buffer) {
            records.push(self.read_capture(capture));
        }
        Ok(records)
    }
}

#[derive(Clone, Debug)]
pub struct Record {
    pub seqid: String,
    pub seq: String,
    pub qual: Quality,
}

impl Random for Record {
    fn rand(len: usize) -> Self {
        let characters = "AGTCAGTCN";//"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        let mut rng = thread_rng();
        let seqid: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(20)
            .map(char::from)
            .collect();
        let seq: String = (0..len)
            .map(|_| {characters.chars().choose(&mut rng).unwrap() as char})
            .collect();
        let qual: Vec<u8> = (0..len)
            .map(|_| {rng.gen_range(33..126) as u8})
            .collect();
        Record {
            seqid,
            seq,
            qual,
        }
    }
}

#[test]
fn test_read_record() {
    let test_normal = r#"
@EAS54_6_R1_2_1_413_324
CCCTTCTTGTCTTCAGCGTTTCTCC
+
;;3;;;;;;;;;;;;7;;;;;;;88
    "#;
    let parser = Parser::new();
    let record = parser.parse_record(test_normal).unwrap();
    assert!(record.seqid == "EAS54_6_R1_2_1_413_324");
    assert!(record.seq == "CCCTTCTTGTCTTCAGCGTTTCTCC");
}

#[test]
fn test_multiline() {
    let test_with_break = r#"
@EAS54_6_R1_2_1_413_324
CCCTTCTTGTCTTCAGCGTTTCTCC
TATTTATTATCTTCAGCGTTTCTCC
+
;;3;;;;;;;;;;;;7;;;;;;;88
;;3;;;;;;;;;;;;7;;;;;;;88
    "#;
    let parser = Parser::new();
    let record = parser.parse_record(test_with_break).unwrap();
    assert!(record.seqid == "EAS54_6_R1_2_1_413_324");
    assert!(record.seq == "CCCTTCTTGTCTTCAGCGTTTCTCCTATTTATTATCTTCAGCGTTTCTCC");

}

#[test]
fn test_read_fails() {
    // TODO: Test quality is read.
    let test_fails = r#"
    @EAS54_6_R1_2_1_413_324
    CCCTTCTTGTCTTCAGCGTTTCTCC
    +
    ;;3;;;;;;;;;;;;7;;;;;;;88
    "#;
    let parser = Parser::new();
    assert!(parser.parse_record(test_fails).is_err());
}
