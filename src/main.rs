use std::fmt;

use anyhow::{Context, Result};
use clap::Parser;
use env_logger;
use log;

mod traits;
mod single_viewer;
mod fastq;
mod widgets;
mod reader;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about=None)]
struct Args {
    /// Read 1
    r1: Option<String>,
    /// Read 2
    r2: Option<String>,
}

#[derive(Debug, Clone)]
struct NotImplemented(String);

impl fmt::Display for NotImplemented {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} not implemented", self.0)
    }
}

fn paired_fastq_viewer(_handle1: &String, _handle2: &String) -> Result<String> {
    anyhow::bail!(NotImplemented(String::from("paired_fastq_viewer")))
}

fn main() -> Result<()> {
    env_logger::init();
    let args = Args::parse();
    log::debug!("Received args {:?}", args);
    let result = match (args.r1, args.r2) {
        (None, None) => anyhow::bail!("No file provided"),
        (Some(r1), None) => single_viewer::main(&r1),
        (Some(r1), Some(r2)) => paired_fastq_viewer(&r1, &r2),
        (None, Some(r2)) => single_viewer::main(&r2),
    };
    let exit = result.with_context(|| format!("Running program"))?;
    println!("{}", exit);
    Ok(())
}
